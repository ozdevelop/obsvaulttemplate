function year_to_str(year, w_quotes = true) {
  year -= 5000;
  p1 = year % 10;
  p2 = ((year % 100) - p1) / 10;
  p3 = (year - p2 * 10 - p1) / 100;
  p12 = p1 + p2 * 10;
  p3 = ["", "ק", "ר", "ש", "ת", "תק", "תר", "תש", "תת", "תתק"][p3];
  if (p12 == 15) {
    p1 = "ו";
    p2 = "ט";
  } else if (p12 == 16) {
    p1 = "ז";
    p2 = "ט";
  } else {
    p1 = ["", "א", "ב", "ג", "ד", "ה", "ו", "ז", "ח", "ט"][p1];
    p2 = ["", "י", "כ", "ל", "מ", "נ", "ס", "ע", "פ", "צ"][p2];
  }
  var ret;
  if (w_quotes) {
    ret = "ה'" + p3 + p2 + p1;
    var l = ret.length-1;
    ret = ret.slice(0, l) + '"' + ret.slice(l);
  } else {
    ret = "ה" + p3 + p2 + p1;
  }
  return ret;
}

module.exports = (w_quotes = true, numeric = false) => {
  var month = [
    "",
    "תשרי",
    "חשוון",
    "כסליו",
    "טבת",
    "שבט",
    "אדר",
    "אדר ב",
    "ניסן",
    "אייר",
    "סיוון",
    "תמוז",
    "אב",
    "אלול",
  ];
  var day_w_quotes = [
    "",
    "א",
    "ב",
    "ג",
    "ד",
    "ה",
    "ו",
    "ז",
    "ח",
    "ט",
    "י",
    'י"א',
    'י"ב',
    'י"ג',
    'י"ד',
    'ט"ו',
    'ט"ז',
    'י"ז',
    'י"ח',
    'י"ט',
    "כ",
    'כ"א',
    'כ"ב',
    'כ"ג',
    'כ"ד',
    'כ"ה',
    'כ"ו',
    'כ"ז',
    'כ"ח',
    'כ"ט',
    "ל",
  ];
  var day_wo_quotes = [
    "",
    "א",
    "ב",
    "ג",
    "ד",
    "ה",
    "ו",
    "ז",
    "ח",
    "ט",
    "י",
    "יא",
    "יב",
    "יג",
    "יד",
    "טו",
    "טז",
    "יז",
    "יח",
    "יט",
    "כ",
    "כא",
    "כב",
    "כג",
    "כד",
    "כה",
    "כו",
    "כז",
    "כח",
    "כט",
    "ל",
  ];
  var day = [];
  if (w_quotes) {
    day = day_w_quotes;
  } else {
    day = day_wo_quotes;
  }

  var nday = new Intl.DateTimeFormat("en-u-ca-hebrew", {
    day: "numeric",
  }).format(new Date());
  var nmonth = new Intl.DateTimeFormat("en-u-ca-hebrew", {
    month: "numeric",
  }).format(new Date());
  var nyear = new Intl.DateTimeFormat("en-u-ca-hebrew", {
    year: "numeric",
  }).format(new Date());

  if (numeric) {
    return (
      nyear.toString() +
      "-" +
      (nmonth > 6 ? nmonth - 1 : nmonth).toString() +
      "-" +
      nday.toString()
    );
  } else {
    var d = day[nday];
    var m = month[nmonth];
    var y = year_to_str(nyear);
    return y + " " + d + " " + m;
  }
};
