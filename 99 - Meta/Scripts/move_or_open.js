var delay=300
async function move(tp, path) {
  await new Promise((r) => setTimeout(r, delay));
  await tp.file.move(path);
}

async function del(app, tfile) {
  await new Promise((r) => setTimeout(r, delay));
  await app.vault.delete(tfile);
}

async function open(app, tfile) {
  await new Promise((r) => setTimeout(r, delay));
  await app.workspace.activeLeaf.openFile(tfile);
}
module.exports = async (tp, tR, app, dir, name) => {
  var path = dir + "/" + name;
  try {
    await tp.file.move(path);
    return "";
  } catch (e) {
    var res = "# !! FILE AREADY EXISTS !!";
    open(app, tp.file.find_tfile(path));
    del(app, tp.file.find_tfile(tp.file.folder(true) + "/" + tp.file.title));
    return res;
  }
};
