---
date: <%tp.date.now("YYYY-MM-DD")%>T<%tp.date.now("HH:mm")%>
tags:
  - שיעור-רבי-חננאל
  - שיעור
cssclasses:
  - base
  - רבי-חננאל
---

<% tp.user.move_or_open(tp, tR, app, "04 - Permanent/שיעורים", "שיעור רבי חננאל " + tp.user.hebdate(false,true) + "-" + tp.date.now("HHmm",0)) %>

# שיעור רבי חננאל
## <% tp.user.hebdate() %>
***
## סיכום השיעור (במהלכו)


---
## סיכום השיעור (לאחריו)


***
## אנקדוטות מהשיעור 
- 

---
### ציטוטים מהשיעור
- 