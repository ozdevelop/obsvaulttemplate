---
date: <%tp.date.now("YYYY-MM-DD")%>T<%tp.date.now("HH:mm")%>
tags:
  - שיעור-רבי-יוסי
  - שיעור
  - אורות-הקודש
cssclasses:
  - base
  - רבי-יוסי
---

<% tp.user.move_or_open(tp, tR, app, "04 - Permanent/שיעורים", "שיעור אוהק רבי יוסי" +
tp.user.hebdate(false,true) + "-" + tp.date.now("HHmm",0)) %>

# שיעור אוה”ק רבי יוסי
## <% tp.user.hebdate() %>
***
## סיכום השיעור (במהלכו)


---
## סיכום השיעור (לאחריו)


***
## אנקדוטות מהשיעור 
- 

---
### ציטוטים מהשיעור
- 