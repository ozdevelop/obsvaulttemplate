---
date: <%tp.date.now("YYYY-MM-DD")%>T<%tp.date.now("HH:mm")%>
tags:
  - שיעור-ראש-הישיבה
  - שיעור
cssclasses:
  - base
  - רהי
---

<% tp.user.move_or_open(tp, tR, app, "04 - Permanent/שיעורים", "שיעור ראש הישיבה " + tp.user.hebdate(false,true) + "-" + tp.date.now("HHmm",0)) %>

# שיעור ראש הישיבה
## <% tp.user.hebdate() %>
***
## סיכום השיעור (במהלכו)


---
## סיכום השיעור (לאחריו)


***
## אנקדוטות מהשיעור 
- 

---
### ציטוטים מהשיעור
- 