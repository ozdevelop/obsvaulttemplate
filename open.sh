#!/usr/bin/bash

## Just a simple way to open the vault and automatically pull and push the changes, better to use the inbuilt git plugin

echo pulling...
git pull
echo done
echo starting obsidian...
echo
obsidian
echo
echo adding to tracking...
git add .
echo committing...
git commit -m "${1:-Update Repo}"
echo pushing...
git push
echo done
